#!/bin/bash

#device=$1
device=/dev/nvme0n1p3 # this is the root device 

# English
sed -i 's/#\(en_US.UTF-8 UTF-8\)/\1/' /etc/locale.gen
sed -i 's/#\(en_US ISO-8859-1\)/\1/' /etc/locale.gen

# Espanol
#sed -i 's/#\(es_DO.UTF-8 UTF-8\)/\1/' /etc/locale.gen
#sed -i 's/#\(es_DO ISO-8859-1\)/\1/' /etc/locale.gen
locale-gen

#Set language (US)
echo "LANG=en_US.UTF-8" > /etc/locale.conf

#Set timezone (Santo Domingo)
ln -sf /usr/share/zoneinfo/America/Santo_Domingo /etc/localtime
hwclock --systohc

#echo "KEYMAP=de_CH-latin1" >> /etc/vconsole.conf
echo "arch" >> /etc/hostname
echo "127.0.0.1		localhost" >> /etc/hosts
echo "::1		    localhost" >> /etc/hosts
echo root:password | chpasswd

#Add new user (remenber not capital letter).
useradd -G wheel -m archinux
echo archinux:password | chpasswd
echo "%wheel ALL=(ALL) ALL" >> /etc/sudoers

sed -i 's/#\(ParallelDownloads = 5\)/\1/' /etc/pacman.conf

pacman -S --noconfirm gdm xorg gnome gnome-extra gnome-tweaks gnome-text-editor remmina vpnc networkmanager-vpnc archlinux-wallpaper make cmake meson gstreamer fakeroot binutils jq nasm gcc gst-libav firefox obs-studio vlc dina-font tamsyn-font ttf-bitstream-vera ttf-croscore ttf-dejavu ttf-droid gnu-free-fonts ttf-ibm-plex ttf-liberation ttf-linux-libertine noto-fonts ttf-roboto tex-gyre-fonts ttf-ubuntu-font-family ttf-anonymous-pro ttf-cascadia-code ttf-fantasque-sans-mono ttf-fira-mono ttf-hack ttf-fira-code ttf-inconsolata ttf-jetbrains-mono ttf-monofur adobe-source-code-pro-fonts cantarell-fonts inter-font ttf-opensans gentium-plus-font ttf-junicode adobe-source-han-sans-otc-fonts adobe-source-han-serif-otc-fonts noto-fonts-cjk noto-fonts-emoji

bootctl install --esp-path=/boot

echo "title Arch Linux" >> /boot/loader/entries/arch.conf
echo "linux /vmlinuz-linux" >> /boot/loader/entries/arch.conf
echo "initrd /initramfs-linux.img" >> /boot/loader/entries/arch.conf
echo "options root=PARTUUID=$(blkid -s PARTUUID -o value $device) rw" >> /boot/loader/entries/arch.conf

mkinitcpio -p linux

systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable cups.service
systemctl enable sshd
systemctl enable avahi-daemon
systemctl enable tlp # You can comment this command out if you didn't install tlp, see above
systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable libvirtd
systemctl enable firewalld
systemctl enable acpid
systemctl enable gdm

printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"
rm /gnome-archroot.sh
