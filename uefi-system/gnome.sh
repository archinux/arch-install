#!/bin/bash
#variables para nvme0n1 # echo ${boot:0:8} # /dev/sdX
device=/dev/nvme0n1
boot=/dev/nvme0n1p1
swap=/dev/nvme0n1p2
root=/dev/nvme0n1p3
#home=/dev/nvme0n1p4

#variables # echo ${boot:0:8} # /dev/sdX
#device=/dev/sdX
#boot=/dev/sdX1
#swap=/dev/sdX2
#root=/dev/sdX3
#home=/dev/sdX4

#make disk gpt partition table

mkfs.vfat $boot
mkfs.ext4 $root
#mkfs.ext4 $home
mkswap $swap

mount $root /mnt
mkdir /mnt/boot
mount $boot /mnt/boot
#mkdir /mnt/home
#mount $home /mnt/home
swapon $swap

sed -i 's/#\(ParallelDownloads = 5\)/\1/' /etc/pacman.conf

pacman -Sy
pacman -S archlinux-keyring

pacstrap /mnt efibootmgr base linux linux-headers linux-docs linux-firmware neofetch networkmanager network-manager-applet dialog wpa_supplicant net-tools mtools dosfstools base-devel avahi xdg-user-dirs xdg-utils gvfs gvfs-smb nfs-utils inetutils dnsutils bluez bluez-utils cups hplip alsa-utils pipewire pipewire-alsa pipewire-pulse pipewire-jack bash-completion openssh rsync reflector acpi acpi_call tlp edk2-ovmf bridge-utils dnsmasq vde2 openbsd-netcat firewalld  sof-firmware nss-mdns acpid os-prober ntfs-3g terminus-font net-tools git sudo nano vim

cp gnome-archroot.sh /mnt/gnome-archroot.sh
chmod +x /mnt/gnome-archroot.sh

genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt ./gnome-archroot.sh $device
