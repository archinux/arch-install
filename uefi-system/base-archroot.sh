#!/bin/bash

#variables
device=$1

# English
sed -i 's/#\(en_US.UTF-8 UTF-8\)/\1/' /etc/locale.gen
sed -i 's/#\(en_US ISO-8859-1\)/\1/' /etc/locale.gen

# Espanol
sed -i 's/#\(es_DO.UTF-8 UTF-8\)/\1/' /etc/locale.gen
sed -i 's/#\(es_DO ISO-8859-1\)/\1/' /etc/locale.gen
locale-gen

#Set language (US)
echo "LANG=en_US.UTF-8" > /etc/locale.conf

#Set timezone (Santo Domingo)
ln -sf /usr/share/zoneinfo/America/Santo_Domingo /etc/localtime
hwclock --systohc

#echo "KEYMAP=de_CH-latin1" >> /etc/vconsole.conf
echo "arch" > /etc/hostname
echo "127.0.0.1		localhost" > /etc/hosts
echo "::1		    localhost" >> /etc/hosts
echo root:password | chpasswd

#Add new user (remenber not capital letter).
useradd -G wheel -m archlinux
echo archlinux:password | chpasswd
echo "%wheel ALL=(ALL) ALL" >> /etc/sudoers

sed -i 's/#\(ParallelDownloads = 5\)/\1/' /etc/pacman.conf

bootctl install --esp-path=/boot

echo "title Arch Linux" >> /boot/loader/entries/arch.conf
echo "linux /vmlinuz-linux" >> /boot/loader/entries/arch.conf
echo "initrd /initramfs-linux.img" >> /boot/loader/entries/arch.conf
echo "options root=PARTUUID=$(blkid -s PARTUUID -o value $device) rw" >> /boot/loader/entries/arch.conf

mkinitcpio -p linux

systemctl enable NetworkManager
systemctl enable sshd

printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"
rm /base-archroot.sh
