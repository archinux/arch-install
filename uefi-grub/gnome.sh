#!/bin/bash

#variables # echo ${boot:0:8} # /dev/sdX
device=/dev/sdX
boot=/dev/sdX1
swap=/dev/sdX2
root=/dev/sdX3
#home=/dev/sdX4

#make disk gpt partition table

mkfs.vfat -F32 $boot
mkfs.ext4 $root
#mkfs.ext4 $home
mkswap $swap

mount $root /mnt
mkdir /mnt/boot
mount $boot /mnt/boot
#mkdir /mnt/home
#mount $home /mnt/home
swapon $swap

sed -i 's/#\(ParallelDownloads = 5\)/\1/' /etc/pacman.conf

pacstrap /mnt grub efibootmgr base linux linux-firmware net-tools networkmanager network-manager-applet openssh neofetch ntfs-3g git sudo nano

cp gnome-archroot.sh /mnt/gnome-archroot.sh
chmod +x /mnt/gnome-archroot.sh

genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt ./gnome-archroot.sh $device