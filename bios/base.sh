#!/bin/bash

#variables
device=/dev/sdX
boot=/dev/sdX1
swap=/dev/sdX2
root=/dev/sdX3
#home=/dev/sdX4

#make disk dos partition table

mkfs.ext2 $boot
mkfs.ext4 $root
#mkfs.ext4 $home
mkswap $swap

mount $root /mnt
mkdir /mnt/boot
#mkdir /mnt/home
mount $boot /mnt/boot
#mount $home /mnt/home
swapon $swap

sed -i 's/#\(ParallelDownloads = 5\)/\1/' /etc/pacman.conf

pacstrap /mnt grub base linux linux-firmware net-tools networkmanager network-manager-applet openssh neofetch ntfs-3g git sudo nano

cp base-archroot.sh /mnt/base-archroot.sh
chmod +x /mnt/base-archroot.sh

genfstab -U /mnt >> /mnt/etc/fstab

arch-chroot /mnt ./base-archroot.sh $device