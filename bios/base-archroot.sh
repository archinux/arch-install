#!/bin/bash

device=$1

# English
sed -i 's/#\(en_US.UTF-8 UTF-8\)/\1/' /etc/locale.gen
sed -i 's/#\(en_US ISO-8859-1\)/\1/' /etc/locale.gen

# Espanol
sed -i 's/#\(es_DO.UTF-8 UTF-8\)/\1/' /etc/locale.gen
sed -i 's/#\(es_DO ISO-8859-1\)/\1/' /etc/locale.gen
locale-gen

#Set language (US)
echo "LANG=en_US.UTF-8" > /etc/locale.conf

#Set timezone (Santo Domingo)
ln -sf /usr/share/zoneinfo/America/Santo_Domingo /etc/localtime
hwclock --systohc

echo "linux" > /etc/hostname
echo "127.0.0.1		localhost" > /etc/hosts
echo "::1		localhost" >> /etc/hosts
echo root:root | chpasswd

#Add new user (remenber not capital letter).
useradd -G wheel -m arch
echo arch:arch | chpasswd
echo "%wheel ALL=(ALL) ALL" >> /etc/sudoers

sed -i 's/#\(ParallelDownloads = 5\)/\1/' /etc/pacman.conf

grub-install $device
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager
systemctl enable sshd

printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"
rm /base-archroot.sh
#echo "desea reiniciar ahora?"
#read confirm
#if [ -$confirm ]
#    printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"
#else
#    rm /base-archroot.sh
#exit && umount -a && reboot

